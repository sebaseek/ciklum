export interface ITableProps {
    data: {
        columns: IDataColumns[];
        rows: IDataRows[];
    };
}

export interface IDataColumns {
    id: string;
    title: string;
}

export interface IDataRows {
    number: number;
    productionBudget?: number;
    releaseDate: string;
    title: string;
    worldwideBoxOffice?: number;
}
