import React, {
    ChangeEvent,
    RefObject,
    useEffect,
    useRef,
    useState
} from 'react';
import { Currency, Locales, Sort } from './Enums';
import { IDataColumns, IDataRows, ITableProps } from './interfaces';
import './Table.css';
import { TableType } from './types';

const Table: TableType = (props: ITableProps): React.ReactElement => {
    // Destructure data property from props.
    const { columns, rows } = props.data;
    const [tableRows, setTableRows] = useState(rows);

    // Add Table Header reference.
    const tableHeaderRowReference: RefObject<any> = useRef(null);

    // Possible values:
    // Sort.ASC - Sort.DESC - Sort.DEFAULT == NO SORT.
    const [sort, setSort] = useState(Sort.DEFAULT);

    // Sorting Constants
    const sortAscendingClass: string = 'icon-sort icon-sort-asc';
    const sortDescendingClass: string = 'icon-sort icon-sort-desc';

    const [searchTerm, setSearchTerm] = useState('');

    useEffect(() => {
        const results: IDataRows[] = rows.filter(row => {
            return row.title.toLowerCase().includes(searchTerm);
        });
        setTableRows(results);
    }, [setSearchTerm, searchTerm]);

    const renderColumns: () => React.ReactFragment = () => {
        return (
            <>
                <tr ref={tableHeaderRowReference}>
                    {columns.map((column: IDataColumns, index: number) => {
                        return (
                            <th
                                key={index}
                                onClick={() =>
                                    onColumnHeaderClick(column, index)
                                }
                            >
                                {column.title}
                                <span />
                            </th>
                        );
                    })}
                </tr>
            </>
        );
    };

    const formatNumberToCurrency: (
        currency: Currency,
        locale: Locales,
        value: number
    ) => string = (currencyEnum, localeEnum, value) => {
        return value.toLocaleString(localeEnum, {
            currency: currencyEnum,
            style: 'currency'
        });
    };

    const renderRows: () => React.ReactFragment = () => {
        return (
            <>
                {tableRows.map((row: IDataRows, index: number) => {
                    return (
                        <tr key={index}>
                            <td>{row.number}</td>
                            <td>{row.title}</td>
                            <td>{row.releaseDate}</td>
                            <td>
                                {row.productionBudget
                                    ? formatNumberToCurrency(
                                          Currency.USD,
                                          Locales.EN_ES,
                                          row.productionBudget
                                      )
                                    : ''}
                            </td>
                            <td>
                                {row.worldwideBoxOffice
                                    ? formatNumberToCurrency(
                                          Currency.USD,
                                          Locales.EN_ES,
                                          row.worldwideBoxOffice
                                      )
                                    : ''}
                            </td>
                        </tr>
                    );
                })}
            </>
        );
    };

    const sortRows: (column: IDataColumns, index: number) => void = (
        column,
        index
    ) => {
        let rowsCopy: IDataRows[];
        rowsCopy = rows.sort((a: any, b: any) => {
            // Compare value 'a' with 'b' based on object property.
            if (a[column.id] && b[column.id]) {
                return Sort.ASC === sort
                    ? a[column.id] > b[column.id]
                        ? 1
                        : -1
                    : Sort.DESC === sort
                    ? a[column.id] < b[column.id]
                        ? 1
                        : -1
                    : 0;
            }
            return 0;
        });

        if (rowsCopy) {
            setTableRows(rowsCopy);
        }
    };

    const onColumnHeaderClick: (column: IDataColumns, index: number) => void = (
        column,
        index
    ) => {
        // Check if there is a reference
        if (tableHeaderRowReference && tableHeaderRowReference.current) {
            // Clean other header sorting references
            tableHeaderRowReference.current.childNodes.forEach((child: any) => {
                if (child.textContent !== column.title) {
                    child.children[0].className = '';
                }
            });
            // Compare children node table header class and apply sorting depending on child status
            if (
                tableHeaderRowReference.current.childNodes[index].children[0]
                    .className === sortAscendingClass
            ) {
                setSort(Sort.DESC);
                tableHeaderRowReference.current.childNodes[
                    index
                ].children[0].className = sortDescendingClass;
            } else if (
                tableHeaderRowReference.current.childNodes[index].children[0]
                    .className === sortDescendingClass
            ) {
                setSort(Sort.ASC);
                tableHeaderRowReference.current.childNodes[
                    index
                ].children[0].className = sortAscendingClass;
            } else {
                setSort(Sort.ASC);
                tableHeaderRowReference.current.childNodes[
                    index
                ].children[0].className = sortAscendingClass;
            }
        }

        // Sort Rows depending on column clicked.
        sortRows(column, index);
    };

    const handleChange: (
        event: ChangeEvent<HTMLInputElement>
    ) => void = event => {
        setSearchTerm(event.target.value);
    };

    return (
        <>
            {
                <div className="data-container">
                    <input
                        type="text"
                        className="input-search"
                        placeholder="Search"
                        value={searchTerm}
                        onChange={handleChange}
                    />
                    <table className="data-table">
                        <thead>{renderColumns()}</thead>
                        <tbody>{renderRows()}</tbody>
                    </table>
                </div>
            }
        </>
    );
};

export default Table;
