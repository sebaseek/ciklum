import React from 'react';
import { ITableProps } from './interfaces';

export type TableType = React.FC<ITableProps>;
