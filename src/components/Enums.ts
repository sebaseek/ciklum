export enum Locales {
    EN_ES = 'en-ES',
    EN_US = 'en-US'
}

export enum Currency {
    USD = 'USD',
    EUR = 'EUR'
}

export enum Sort {
    ASC = 'ASC',
    DESC = 'DESC',
    DEFAULT = ''
}
