// React
import React from 'react';

// Custom Styles
import './App.css';
import Table from './components/Table';

// Import JSON
import data from './data.json';

const App: React.FC = () => {
    return (
        <>
            <Table data={data} />
        </>
    );
};

export default App;
